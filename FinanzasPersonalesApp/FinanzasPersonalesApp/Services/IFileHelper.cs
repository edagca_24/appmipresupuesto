﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinanzasPersonalesApp.Services
{
    interface IFileHelper
    {
        string GetLocalFilePath(string fileName);
    }
}
