﻿using FinanzasPersonalesApp.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace FinanzasPersonalesApp.ViewModels
{
    public class MainPageViewModel 
    {
        public ICommand ToLoginCommand { protected set; get; }
        public MainPageViewModel()
        {
            ToLoginCommand = new Command(ToLogin);
        }
        public void ToLogin()
        {
           
        }
    }
}
