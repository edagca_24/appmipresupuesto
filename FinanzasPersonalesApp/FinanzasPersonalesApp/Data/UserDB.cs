﻿using FinanzasPersonalesApp.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace FinanzasPersonalesApp.Data
{
    public class UserDB
    {
        readonly SQLiteAsyncConnection _SQLiteConnection;

        public UserDB(string dbPath)
        {
            _SQLiteConnection = new SQLiteAsyncConnection(dbPath);
            _SQLiteConnection.CreateTableAsync<User>().Wait();
        }

        public async Task<List<User>> GetUsers()
        {
            return await _SQLiteConnection.Table<User>().ToListAsync();
        }
        public async Task<User> GetSpecificUser(int id)
        {
            return await _SQLiteConnection.Table<User>().FirstOrDefaultAsync(t => t.Id == id);
        }
        public Task<int> DeleteUser(int id)
        {
            return _SQLiteConnection.DeleteAsync(id);
        }
        public async Task<string> AddUser(User user)
        {
            var data = _SQLiteConnection.Table<User>();
            var d1 = data.Where(x => x.Id == user.Id && x.UserName == user.UserName).FirstOrDefaultAsync();
            if (d1 == null)
            {
                await _SQLiteConnection.InsertAsync(user);
                return "Sucessfully Added";
            }
            else
                return "Already Mail id Exist";
        }      
       
        public async Task<bool> LoginValidate(string userName1, string pwd1)
        {
            var data = _SQLiteConnection.Table<User>();
            var d1 = data.Where(x => x.UserName == userName1 && x.Password == pwd1).FirstAsync();
            if (d1 != null)
            {
                return true;
            }
            else
                return false;
        }
    }
}
