﻿using FinanzasPersonalesApp.Data;
using FinanzasPersonalesApp.Models;
using FinanzasPersonalesApp.Services;
using FinanzasPersonalesApp.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinanzasPersonalesApp
{
    public partial class App : Application
    {
        private static UserDB databaseUser;
        public static UserDB DatabaseUser
        {
            get 
            {
                if (databaseUser == null)
                {
                    databaseUser = new UserDB(DependencyService.Get<IFileHelper>().GetLocalFilePath("UserDb.Db3"));
                }
                return databaseUser;
            }
        }

        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new MainPage());
         
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
